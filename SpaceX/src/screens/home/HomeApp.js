import React, { Fragment, useState, useEffect }from 'react';
import {
    ScrollView,
    TouchableOpacity,
    View,
    Text
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {
    Cabecalho,
    CabecalhoText,
} from './styles/stylesHome';

import UiMissionsList from '../../shared-components/MissionsList';
import Input from '../../shared-components/Input';

export default function HomeApp({missions, onChange, value, navigation}){
    const [page, setPage] = useState(0);
    const [limite, setLimite] = useState(6);
    const [offSet, setOffset] = useState(0);
    const [quantPages, setQuantpages] = useState(0);

    useEffect(() => {
        calcOffset();
    }, [page]);

    useEffect(()=>{
        let aux=Math.floor(missions.length/limite);

        setQuantpages(aux);
    }, [missions]);

    function incrementPage(){
        setPage(page+1);
    }

    function decrementPage(){
        if(page>0)
            setPage(page-1);
    }

    function calcOffset(){
        let aux= page*limite;
        setOffset(aux);
    }

    return (
        <Fragment>
            <Cabecalho>
                <CabecalhoText>SpaceX</CabecalhoText>
            </Cabecalho>

            <Input onChange={onChange} value={value}/>

            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={{flex:1}}>
                    <UiMissionsList missions={missions} navigation={navigation} offSet={offSet} limite={limite}/>
            </ScrollView>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <TouchableOpacity disabled={(page==0)} onPress={decrementPage} style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <MaterialIcons name="keyboard-arrow-left" size={28} color="#999" />
                    <Text>Anterior</Text>
                </TouchableOpacity>
                <View>
                    <Text>page: {(page+1)}</Text>
                </View>
                <TouchableOpacity disabled={!((page+1)<=quantPages)} onPress={incrementPage} style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text>Próximo</Text>
                    <MaterialIcons name="keyboard-arrow-right" size={28} color="#999" />
                </TouchableOpacity>
            </View>
        </Fragment>
    );
}
