import React from 'react'
import styled from 'styled-components/native';

export const Cabecalho = styled.View`
    background: #021526;

    display: flex;
    justify-content: space-between;
    align-items: center;

`;

export const CabecalhoText = styled.Text`
    font-weight: bold;
    color: #fff;
    font-size: 22px;
    margin: 10px;
    margin-top: 20px;
    margin-bottom: 10px;
`;