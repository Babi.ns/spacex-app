import React, { useState, useEffect } from 'react';
import HomeApp from './HomeApp';
import { useSelector, useDispatch } from 'react-redux';
import * as ActionsApplication from '../../store/application/actions/missions.action';
import * as ActionsHome from './store/actions/home.action';

function ScreenHome(props){

    const dispatch = useDispatch();
    const missionsRedux = useSelector( state => state.missionsReducer.missions );
    const searchText = useSelector( state => state.homeReducer.searchText );

    const [missions, setMissions] = useState([]);

    useEffect(()=>{
        async function loadMissions(){
            let response = await ActionsApplication.loadMissions();

            dispatch(response);
        }

        loadMissions();

        let response = ActionsHome.setSearchText('');
        dispatch(response);
    },[dispatch]);

    useEffect(()=>{
        if(missionsRedux){
            let aux= missionsRedux.filter(e=> 
                e.mission_name.toLowerCase().includes(searchText.toLowerCase()) || 
                e.rocket.rocket_name.toLowerCase().includes(searchText.toLowerCase()) || 
                searchText === '')
            setMissions(aux);
        }else{
            setMissions([]);
        }
    },[searchText, missionsRedux]);

    function handlerSearch(text){
        let response = ActionsHome.setSearchText(text);
        dispatch(response);
    }

    return (
        <HomeApp missions={missions} onChange={handlerSearch} value={searchText} navigation={props.navigation}/>
    )
}

export default ScreenHome;
