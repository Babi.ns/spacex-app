export const types={
  SET_TEXT_SEARCH:"[SET] TEXT SEARCH"
};

export const setSearchText=(text)=>{
  return {
      type: types.SET_TEXT_SEARCH,
      payload: text
  }
}
