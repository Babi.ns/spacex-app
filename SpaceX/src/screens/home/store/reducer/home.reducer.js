import {types} from '../actions/home.action';

const INITIAL_STATE={
    searchText:''
};

export function homeReducer(state = INITIAL_STATE, action){
    switch(action.type){
        case types.SET_TEXT_SEARCH:
            return  {...state, searchText: action.payload};
        default:
            return state;
    }
}
