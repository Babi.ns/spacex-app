import React, { useEffect, useState } from 'react';
import WebApp from './WebApp';

export default function ScreenWeb(props){
    const [link, setLink] = useState(null);
    const [name, setName] = useState(null);

    useEffect(()=>{
        let link = props.navigation.getParam('link', null);
        let name = props.navigation.getParam('name', null);

        setLink(link);
        setName(name);
    },[]);
    
    return (
        <WebApp link={link} name={name}/>
    );
}
