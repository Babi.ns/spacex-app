import React, { useState} from 'react';
import {
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import {
    Cabecalho,
    CabecalhoText,
} from './styles/stylesWeb';

import WebView from 'react-native-webview';

export default function WebApp ({link, name}){

    return(
        <>
            <Cabecalho>
                <CabecalhoText>
                    {name}
                </CabecalhoText>
            </Cabecalho>
            <WebView
                source={{uri: link}} />
        </>
    );
}
