import React, { useEffect } from 'react';
import { useSelector, useDispatch} from 'react-redux';
import DetailsApp from './DetailsApp';

import * as ActionsDetails from './store/actions/details.action';

export default function ScreenDetails(props){
    const dispatch = useDispatch();
    const mission = useSelector(state => state.detailsReducer.mission);

    useEffect(()=>{
        async function loadMission(){
            let id = props.navigation.getParam('itemId', null);
            let response = await ActionsDetails.fetchDetailsMission(id);

            dispatch(response);
        }

        loadMission();
    },[dispatch]);

    return (
        <DetailsApp mission={mission} navigation={props.navigation}/>
    );
}
