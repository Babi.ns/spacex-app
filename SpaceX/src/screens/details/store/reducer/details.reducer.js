import {types} from '../actions/details.action';

const INITIAL_STATE={
    mission:null
};

export function detailsReducer(state = INITIAL_STATE, action){
    switch(action.type){
        case types.SET_MISSION_DETAILS:
            return  {...state, mission: action.payload};
        default:
            return state;
    }
}
