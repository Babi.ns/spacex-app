import {api} from '../../../../services/api';

export const types={
    SET_MISSION_DETAILS:"[SET] MISSION DETAILS"
};

export const fetchDetailsMission= async (id)=>{

    if(!id)
        throw new Error('Id da missão não enviada');

    let response = await api.get(`/launches/${id}`);

    return {
        type: types.SET_MISSION_DETAILS,
        payload: response.data
    }
}
