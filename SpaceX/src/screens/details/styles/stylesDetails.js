import React from 'react'
import styled from 'styled-components/native';

export const Container = styled.View`
    background: white;
    margin: 10px;
    display: flex;
    flex-wrap: nowrap;
    justify-content: space-between;
`;
export const Cabecalho = styled.View`
    background: #021526;
`;

export const CabecalhoText = styled.Text`
    font-weight: bold;
    color: #fff;
    font-size: 22px;
    margin: 10px;
    margin-top: 20px;
    margin-bottom: 10px;
`;

export const Title = styled.Text`
    font-weight: bold;
    font-size: 16px;
    margin-top: 6px;
`;

export const Subtitle = styled.Text`
    font-weight: bold;
    font-size: 16px;
    color: gray;
`;

export const SubtitleSpecial = styled.Text`
    font-weight: bold;
    font-style: italic;
    font-size: 18px;
    color: #021526;
    margin-top: 10px;
    margin-bottom: 20px;
`;

export const ContainerWebView = styled.TouchableOpacity`
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;