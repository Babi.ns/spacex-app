import React, { useState } from 'react';
import {
    ScrollView,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    Cabecalho,
    CabecalhoText,
    Title,
    Subtitle,
    SubtitleSpecial,
    ContainerWebView
} from './styles/stylesDetails';

import configYou from '../../../config.json';

import YouTube from 'react-native-youtube';
import WebView from 'react-native-webview';

export default function DetailsApp({mission, navigation}){

    const [isReady, setIsRead] =useState(true);
    const [status, setStatus] =useState(null);
    const [quality, setQuality] =useState(null);
    const [error, setError] =useState(null);

    return(
        <>
            {mission
                ?(
                    <>
                        <ScrollView>
                            <Cabecalho>
                                <CabecalhoText>Detalhes</CabecalhoText>
                            </Cabecalho>

                            <Container>
                                <Title>Missão: <Subtitle>{mission.mission_name}</Subtitle></Title>
                                <Title>Ano: <Subtitle>{mission.launch_year}</Subtitle></Title>
                                <Title>Foguete: <Subtitle>{mission.rocket.rocket_name}</Subtitle></Title>
                                <Title>Tipo: <Subtitle>{mission.rocket.rocket_type}</Subtitle></Title>
                                
                                {/* <Subtitle>{mission.rocket.second_stage.payloads.nationality}</Subtitle> */}

                                <Title>Local: <Subtitle>{mission.launch_site.site_name}</Subtitle></Title>
                                
                                <ContainerWebView onPress={()=>{navigation.navigate("Web",{
                                    link: mission.links.article_link,
                                    name: mission.mission_name
                                })}}>
                                    <SubtitleSpecial>Artigo: <Subtitle>{mission.mission_name}</Subtitle></SubtitleSpecial>
                                </ContainerWebView>

                                <YouTube
                                    videoId={mission.links.youtube_id}   
                                    play={true}             
                                    fullscreen={false}       
                                    loop={false}            
                                    apiKey={configYou.ApiKey}

                                    onReady={e => setIsRead(true)}
                                    onChangeState={e => setStatus(e.state)}
                                    onChangeQuality={e => setQuality(e.quality)}
                                    onError={e => setError(e.error)}
                                    style={{ alignSelf: 'stretch', height: 200 }}
                                />
                            </Container>
                        </ScrollView>
                    </>
                )
                :<Subtitle>carregando missão</Subtitle>}
        </>
    );
}

