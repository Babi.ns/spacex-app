import React from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";

import ScreenHome from '../screens/home';
import ScreenDetails from '../screens/details';
import ScreenWeb from '../screens/web';

const AppNavigator = createStackNavigator({
    Home: {
        screen: ScreenHome
    },
    Details: {
        screen: ScreenDetails
    },
    Web: {
        screen: ScreenWeb
    },
},{
    initialRouteName:'Home',
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
});

const Routes = createAppContainer(AppNavigator);

export default Routes;
