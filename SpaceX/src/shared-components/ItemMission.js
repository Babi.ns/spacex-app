import React, { Fragment }from 'react';
import {
    Container,
    Title,
    Subtitle,
    DescriptioTitle,
    RowCenter,
    Column,
    ImageItem,
    Divider
} from './styles/itemMission';

export default function UiItemMission({mission, navigation}){
    return (
        <Fragment>
            <Container onPress={()=>navigation.navigate('Details',{
                    itemId: mission.flight_number,
                })}>
                <RowCenter>
                    <ImageItem source={{uri: mission.links.mission_patch}}></ImageItem>
                    <Column>
                        <Title>{mission.mission_name}</Title>
                        <RowCenter>
                            <Subtitle>{mission.rocket.rocket_name}</Subtitle>
                            <Subtitle>{mission.launch_year}</Subtitle>
                        </RowCenter>
                        {mission.details && <DescriptioTitle numberOfLines={1}>{mission.details.slice(0,100)}...</DescriptioTitle>}
                    </Column>
                </RowCenter>
            </Container>
            <Divider/>
        </Fragment>
    );
}
