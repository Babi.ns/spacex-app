import React from 'react'
import styled from 'styled-components/native'

export const Container = styled.TouchableOpacity`
    background: white;
    padding: 5px 10px;
`;

export const Row = styled.View`
    display: flex;
    flex-direction: row;
`;

export const RowCenter = styled(Row)`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Column = styled.View`
    display: flex;
    flex-direction: column;
`;

export const ColumnCenter = styled.View`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const Title = styled.Text`
    font-weight: bold;
    font-size: 18px;
`;

export const Subtitle = styled.Text`
    font-weight: bold;
    font-size: 14px;
    color:gray;
    margin-right: 5px;
`;

export const DescriptioTitle = styled.Text`
    font-weight: bold;
    font-size: 12px;
    color:gray;
    display:flex;
    width:60%;
`;

export const ImageItem = styled.Image`
    width: 50px;
    height: 50px;
    margin: 10px;
    margin-left: 0;
    border-radius: 10px;
`;

export const Divider = styled.View`
    height: 1px;
    display: flex;
    flex: 1;
    background: gray;
`;
