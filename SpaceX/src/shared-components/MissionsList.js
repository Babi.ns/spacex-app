import React, { Fragment }from 'react';
import {
    Text
} from 'react-native';
import {useSelector} from 'react-redux';

import UiItemMission from './ItemMission';

export default function UiMissionsList({missions, navigation, offSet, limite}){

    return (
        <Fragment>
            {missions && missions.length > 0
                ? missions.slice(offSet, (offSet+limite)).map((e, index) => {
                    return <UiItemMission key={index} mission={e} navigation={navigation}/>
                })
                : <Text>Sem missões lançadas</Text>
            }
        </Fragment>
    );
}
