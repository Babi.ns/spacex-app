import React, {useState} from 'react';
import {
    TextInput
} from 'react-native';

export default function Input({onChange, value}){
    return (
        <TextInput
                style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                onChangeText={(text) => onChange(text)}
                value={value}
                placeholder="Busque uma missão"
            />
    );
}
