import { api } from '../../../services/api';

export const types={
    GET_MISSIONS:"[GET] MISSIONS"
};

export const loadMissions= async () => {
    let response = await api.get('/launches');

    return {
        type: types.GET_MISSIONS,
        payload: response.data
    }
}
