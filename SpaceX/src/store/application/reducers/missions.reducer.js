import {types} from '../actions/missions.action';

const INITIAL_STATE = {
    missions: null
};

export function reducerMissions(state = INITIAL_STATE, action){
    switch(action.type){
        case types.GET_MISSIONS:
            return  {...state, missions:action.payload};
        default:
            return state;
    }
}