import { combineReducers } from 'redux';

import { reducerMissions } from './application/reducers/missions.reducer';
import { homeReducer } from '../screens/home/store/reducer/home.reducer';
import { detailsReducer } from '../screens/details/store/reducer/details.reducer';

const reducer = combineReducers({
    missionsReducer: reducerMissions,
    homeReducer: homeReducer,
    detailsReducer: detailsReducer


});

export default reducer;
