Criar um app React Native
-- Buscar e listar os dados do lançamentos da SpaceX
--- Link da API
https://api.spacexdata.com/v3

Implementações extra
-- Aplicar paginação a lista
-- Mostrar o vídeo do lançamento
-- Abrir uma WebView com o link do artigo lançamento

Campo de pesquisa
--- Filtrar atraves do nome da missão(variavel: mission_name)

Listagem
--- Exibir todos os lançamentos
-- Extra --
---- Paginar a listagem de lançamentos

Detalhes
--- Exibir todos os dados do lançamento(Quando clicado)
---- Dados:
----- flight_number(numero do vôo);
------ Exemplo
Api pega o numero do vôo para pegar a rota(caminho/url) da missão
https://api.spacexdata.com/v3/launches/65

----- mission_name(nome da missão);
----- launch_year(ano do lançamento);
----- rocket_name(nome do foguete);
----- rocket_type(tipo de foguete);
----- nationality(nacionalidade);
----- site_name(nome do local do lançamento);

----- mission_patch(imagem da missão);
----- article_link(link do artigo);
----- video_link(link do video);

-- Extra --
---- Exibir video do lançamento nos detalhes(variavel: video_link)
---- Abrir uma WebView com o link do artigo lançamento(variavel: article_link)